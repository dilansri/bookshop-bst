package main.nsbm.cs3.nukez;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BookRecord {
	
	/* BookRecord fields declaration */
	private String title;
	private String  authorName;
	private String authorSurname;
	private Long ISBN;
	
	/* getters and setters */
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAuthorName() {
		return authorName;
	}
	
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	
	public String getAuthorSurname() {
		return authorSurname;
	}
	
	public void setAuthorSurname(String authorSurname) {
		this.authorSurname = authorSurname;
	}
	
	public Long getISBN() {
		return ISBN;
	}
	
	public void setISBN(Long ISBN) {
		this.ISBN = ISBN;
	}
	
	public String toString(){
		return "["+title+" - "+ISBN+" By: "+authorName+ " "+authorSurname +"]";
	}
	
	public void print(){
		System.out.println(this);
	}
	/** 
	 * Matching the Book Title with the keyword
	 * pass to the method using Regex
	 * @param keyword the pattern that use to match the title
	 * @return if the keyword match return true otherwise return false
	 */
	public boolean matchKeyword(String keyword)
	{
	
		Pattern pattern = Pattern.compile(keyword,Pattern.CASE_INSENSITIVE);

	    Matcher matcher = pattern.matcher(title);
	    
	    boolean matched = false;
	    // Checking whether is there any matched pattern
 	    while (matcher.find())
	    {
	    	matched = true;
	    	break;
	    }
	    
	    return matched;

	}
	
	

}
