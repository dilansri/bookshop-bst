package main.nsbm.cs3.nukez;


public class BST {
	
	private Node root;
	
	/* Inner class representing a Node inside BST */
	private class Node{
		
		Node left;
		Node right;
		String key;
		BookRecord element;
		
		public Node(String key,BookRecord element){
			this.key = key;
			this.element = element;
		}
	}
	
	/**
	 * This method will insert a BookRecord object to the Binary Search Tree.
	 * If a non existing BookRecord is inserted method will return immediately,
	 * without doing any change to the Binary Search Tree.
	 * @param record BookRecord object that need to be inserted.
	 */
	public void insert(BookRecord record){
		if(record == null) return;
		root = insert(root,record.getTitle(),record);
	}
	
	/* actual implementation of insert method */
	private Node insert(Node node,String key,BookRecord element){
		
		/* inserting node at the right place */
		if(node == null)
			return new Node(key,element);
		int comparator = key.compareToIgnoreCase(node.key);
		
		/* comparing key with current node's key and deciding where the new node goes */
		if(comparator < 0) 
			node.left = insert(node.left,key,element);
		else if (comparator > 0)
			node.right = insert(node.right,key,element);
		else
			node.element = element;
		
		return node;
	}
	

	/**
	 * This method will delete a node with specified BookRecord Title.
	 * @param keyword Title of the BookRecord that need to be deleted.
	 */
	public void deleteByName(String keyword){
		
		root = delete(root ,keyword);
	}
	
	private Node delete(Node node, String key) {
		if (node == null) return null;
		/* Finding the node that need to be deleted*/
		int comparator = key.compareToIgnoreCase(node.key);
		if      (comparator < 0) 
			node.left  = delete(node.left,  key);
		else if (comparator > 0) 
			node.right = delete(node.right, key);
		else { 
		/* Represent the 3 cases when deleting node from the Binary search tree*/
			if (node.right == null) 
				return node.left;
			if (node.left  == null) 
				return node.right;
			Node t = node;
			/* Find the minimum node from right sub tree of the deleting node */
			node = min(t.right);
			/*Replacing deleting node with minimum node that found above */
			node.right = deleteMin(t.right);
			node.left = t.left;
		} 

		return node;
	}
	/*removing the minimum node from the given tree and returning the root of new tree */
	private Node deleteMin(Node node) {
		if (node.left == null) return node.right;
		node.left = deleteMin(node.left);
		return node;
	}
	/*gives the reference of minimum node in the tree */
	private Node min(Node node) { 
		if (node.left == null) return node; 
		else                
			return min(node.left); 
	}
	/**
	 * Deleting a node specified by its ISBN
	 * @param ISBN ISBN value of the BookRecord that need to be deleted.
	 */
	public void deleteByISBN(Long ISBN){
		root = deleteByISBN(root,ISBN);		
	}
	
	/* Implementation of deleting a node by ISBN. 
	 * Will return the new root.
	 * Tree is traversed using preorder traversal method.  
	 */
	private Node deleteByISBN(Node node,Long ISBN){
		//base condition of preorder traversal
		if(node == null)
			return null;		
		
		//visiting the element. if the ISBN found, node will be deleted. No more traversing.
		if(node.element.getISBN().equals(ISBN))
			return delete(node,node.element.getTitle());
		
		//going left side of the tree
		node.left = deleteByISBN(node.left,ISBN);
		//going right side of the tree
		node.right = deleteByISBN(node.right,ISBN);
		
		return node;
	}
	/** This method will search a record by name on the binary tree 
	  * after calling it's private method "search"
	  * @param title Book title that need tp search in the tree
	  */
	public BookRecord searchByName(String title){
		
		return search(root, title);
	}
	
	//implementation of the search method.
	private BookRecord search(Node node, String key) {
        if (node == null)
        	return null;
		/* Set a comparator for ease to access the binary tree */
        int comparator = key.compareToIgnoreCase(node.key);
		/* Compare all nodes using comparator if detect the right node, It will returned */
        if(comparator < 0) 
        	return search(node.left, key);
        else if (comparator > 0)
        	return search(node.right, key);
        else
        	return node.element;
    }

	/**
	 * Searching a node specified by its ISBN 
	 * @param ISBN ISBN value of the BookRecord that need to be searched.
	 */
	public BookRecord searchByISBN(Long ISBN){
	
		// storing the result node, if there is any
		Node searchResult = searchByISBN(root,ISBN);
		
		// if result node is not null then return BookRecord
		if(searchResult != null)
			return searchResult.element;
		return null ;
	}
	
	// search ISBN using preoder traversal method
	private Node searchByISBN(Node node,Long ISBN){
		if(node == null) return null;
		
		// checking whether ISBNs match
		if(node.element.getISBN().equals(ISBN)){
			return node;
		}
		
		
		// searching the left part of the tree
		Node leftSearch = searchByISBN(node.left,ISBN);
		if(leftSearch != null)
			return leftSearch;
		
		// searching the right part of the tree
		Node rightSearch = searchByISBN(node.right,ISBN);
		if(rightSearch != null)
			return rightSearch;
		
		return null;		
	}
	/**
	 * Printing all the books matching the given keyword
	 * @param keyword the that need to search in the tree
	 *
	 */
	public void printBooks(String keyword){
		print(root,keyword);
	}
	
	private void print(Node node,String keyword){
		if(node == null)
			return;
	   //Traversing the tree using inorder traversal method		
		print(node.left,keyword);
		
		if(node.element.matchKeyword(keyword)){
			node.element.print();
		}
		
		print(node.right,keyword);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
