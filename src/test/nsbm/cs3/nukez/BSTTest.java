package test.nsbm.cs3.nukez;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import main.nsbm.cs3.nukez.BST;
import main.nsbm.cs3.nukez.BookRecord;

import org.junit.Before;
import org.junit.Test;
public class BSTTest {	
	
	BST binarySearchTree ;
	
	@Before
	public void initializing(){
		binarySearchTree = new BST();
		Scanner scanner;
		try{
			scanner = new Scanner(new File("data.txt"));
			scanner.useDelimiter(System.getProperty("line.separator"));
			int count = 0;
			while(scanner.hasNext() && count != 30){
				String line = scanner.next();
				String[] data = line.split(",",4);
				BookRecord record = new BookRecord();
				record.setISBN(Long.parseLong(data[0]));
				record.setAuthorName(data[1]);
				record.setAuthorSurname(data[2]);
				record.setTitle(data[3]);
				binarySearchTree.insert(record);
			}
		}catch(FileNotFoundException fnfe){
			System.err.println("Error in data.txt file");
		}
		
		
	}
	
	@Test
	public void insertedRecordShouldReturnWhenSearchByName(){
		BookRecord record = new BookRecord();		
		record.setTitle("Java Programming Test Book");
		record.setAuthorName("Garry");
		record.setAuthorSurname("Kasparov");
		record.setISBN(1234567890123L);		
		binarySearchTree.insert(record);		
		assertTrue(binarySearchTree.searchByName("Java Programming Test Book").equals(record));
	}
	
	@Test
	public void insertedRecordShouldReturnWhenSearchByISBN(){
		BookRecord record = new BookRecord();		
		record.setTitle("Java Programming Test Book");
		record.setAuthorName("Garry");
		record.setAuthorSurname("Kasparov");
		record.setISBN(1234567890123L);		
		binarySearchTree.insert(record);
		assertTrue(binarySearchTree.searchByISBN(1234567890123L).equals(record));
	}
	
	@Test
	public void deletedByNameRecordShouldNotBeInTheTree(){
		BookRecord record = new BookRecord();		
		record.setTitle("Java Programming Test Book");
		record.setAuthorName("Garry");
		record.setAuthorSurname("Kasparov");
		record.setISBN(1234567890123L);		
		binarySearchTree.insert(record);
		assertNotNull(binarySearchTree.searchByName("Java Programming Test Book"));
		binarySearchTree.deleteByName("Java Programming Test Book");		
		assertNull(binarySearchTree.searchByName("Java Programming Test Book"));
	}
	
	@Test
	public void deletedByISBNRecordShouldNotBeInTheTree(){
		BookRecord record = new BookRecord();		
		record.setTitle("Java Programming Test Book");
		record.setAuthorName("Garry");
		record.setAuthorSurname("Kasparov");
		record.setISBN(1234567890123L);		
		binarySearchTree.insert(record);
		assertNotNull(binarySearchTree.searchByName("Java Programming Test Book"));
		binarySearchTree.deleteByISBN(1234567890123L);		
		assertNull(binarySearchTree.searchByName("Java Programming Test Book"));
	}
	
	public static void main(String[] args)
	{ 
		BST binarySearchTree = new BST();
		
		
		Scanner scanner;
		try{
			scanner = new Scanner(new File("data.txt"));
			scanner.useDelimiter(System.getProperty("line.separator"));
			int count = 0;
			while(scanner.hasNext() && count != 30){
				String line = scanner.next();
				String[] data = line.split(",",4);
				BookRecord record = new BookRecord();
				record.setISBN(Long.parseLong(data[0]));
				record.setAuthorName(data[1]);
				record.setAuthorSurname(data[2]);
				record.setTitle(data[3]);
				binarySearchTree.insert(record);
			}
		}catch(FileNotFoundException fnfe){
			System.err.println("Error in data.txt file");
		}	
		
		
		BookRecord record = new BookRecord();		
		record.setTitle("Java Programming Test Book");
		record.setAuthorName("Garry");
		record.setAuthorSurname("Kasparov");
		record.setISBN(1234567890123L);		
		binarySearchTree.insert(record);
		
		//binarySearchTree.printBooks("test");
		
		binarySearchTree.deleteByName("java programming test book");
		binarySearchTree.printBooks("test");
		
		/*
		
		System.out.println(binarySearchTree.searchByName("Java Programming Intro"));		
		binarySearchTree.deleteByISBN(1234567890124L);
		binarySearchTree.deleteByISBN(1234567890123L);
		System.out.println(binarySearchTree.searchByName("Java Programming Intro"));
		binarySearchTree.printBooks("Java");
		
		System.out.println(binarySearchTree.searchByISBN(1234567890123L)); */
	
	}

}